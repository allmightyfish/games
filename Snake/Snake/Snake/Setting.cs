﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Snake
{
    class Setting
    {
        public static int width { get; set; }
        public static int height { get; set; }
        public static int speed { get; set; }
        public static int score { get; set; }
        public static int points { get; set; }
        public static bool gameOVer { get; set; }


        public Setting()
        {
            width = 20;
            height = 20;
            speed = 20;
            score = 0;
            points = 0;
            gameOVer = false;
        }
    }
}
