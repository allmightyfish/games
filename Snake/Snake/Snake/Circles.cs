﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Snake
{
    class Circles
    {
        public int X { get; set; }
        public int Y { get; set; }

        public Circles()
        {
            X = 0;
            Y = 0;
        }
    }
}
