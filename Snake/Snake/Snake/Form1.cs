﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Snake
{
    public partial class Form1 : Form
    {

        private int points = 0;
        private int direction = 0;
        private List<Circles> Snake = new List<Circles>();
        private Timer gameTimer = new Timer();

        private Circles food = new Circles();
        private SqlConnection conn;
        public Form1()
        {
            
            InitializeComponent();
            conn = new SqlConnection("Server = .\\SQLEXPRESS; DATABASE=HighScore; Integrated Security=TRUE ");
            new Setting();
            userCMDBox.Text = "Press Enter to start!";
           gameTimer.Interval = 1000 / Setting.speed;
            gameTimer.Tick += updateScreen;
            gameTimer.Start();
            startGame();

        }

        private void startGame()
        {
            userCMDBox.Text = "Game has began, good luck!";
            Snake.Clear();
            Setting.gameOVer = false;
            Circles head = new Circles
            {
                X = 15,
                Y = 15
            };
            Snake.Add(head);
            addBlocktoBoard();
        }

        private void updateScreen(object sender, EventArgs e)
        {
            if (Setting.gameOVer)
            {
                if (Input.KeyPress(Keys.Enter))
                {
                   
                    startGame();
                }
            }
            else
            {
                KeyDown += new KeyEventHandler(isKeyDown);
                KeyUp += new KeyEventHandler(isKeyUp);

                if (Input.KeyPress(Keys.Right))
                {
                    direction = 0;
                }
                else if (Input.KeyPress(Keys.Left))
                {
                    direction = 1;
                }
                else if (Input.KeyPress(Keys.Up))
                {
                    direction = 2;
                }
                else if (Input.KeyPress(Keys.Down))
                {
                    direction = 3;
                }
                movePlayer();
            }
            pbCanvas.Invalidate();
        }

       private void movePlayer()
        {

            for (int i = Snake.Count - 1; i >= 0; i--)
            {
                if (i == 0)
                {
                    switch (direction)
                    {
                        case 0:
                            Snake[i].X++;
                            break;
                        case 1:
                            Snake[i].X--;
                            break;
                        case 2:
                            Snake[i].Y--;
                            break;
                        case 3:
                            Snake[i].Y++; ;
                            break;
                    }

                    int maxXpos = pbCanvas.Size.Width / Setting.width;
                    int maxYpos = pbCanvas.Size.Height / Setting.height;
                    if (Snake[i].X > maxXpos || Snake[i].X < 0 || Snake[i].Y > maxYpos || Snake[i].Y < 0)
                    {
                        endGame();
                    }

                    for (int j = 1; j < Snake.Count; j++)
                    {
                        if (Snake[i].X == Snake[j].X && Snake[i].Y == Snake[j].Y)
                        { 
                            endGame();
                        }
                    }

                    if (Snake[0].X == food.X && Snake[0].Y == food.Y)
                    {
                        eat();
                    }
                }
                else
                {
                    Snake[i].X = Snake[i - 1].X;
                    Snake[i].Y = Snake[i - 1].Y;
                }
            }
            

        }

        void isKeyDown (object sender, KeyEventArgs e)
        {
            Input.changeState(e.KeyCode, true);
        }
        void isKeyUp(object sender, KeyEventArgs e)
        {
            Input.changeState(e.KeyCode, false);
        }

        private void updateGraphics(object sender, PaintEventArgs e)
        {
            Graphics canvas = e.Graphics; //Create newgraphics class

            if (!Setting.gameOVer)
            {
                Brush snakeColor;
                for (int i = 0; i < Snake.Count; i++)
                {
                    if (i == 0)
                    {
                        snakeColor = Brushes.Black;
                    }
                    else
                    {
                        snakeColor = Brushes.Green;
                    }
                    canvas.FillEllipse(snakeColor,
                                        new Rectangle(
                                                Snake[i].X * Setting.width,
                                                Snake[i].Y * Setting.height,
                                                Setting.width, Setting.height
                                                ));
                    canvas.FillEllipse(Brushes.Red,
                                        new Rectangle(
                                                food.X * Setting.width,
                                                food.Y * Setting.height,
                                                Setting.width, Setting.height
                                                ));
                }
            }
            else
            {
                userCMDBox.Text = "Game Over \n" + "Final Score is " + Setting.score + "! Press enter to play again!";
                pointsBox.Text = "Points: 0";
            }

        }



        private void addBlocktoBoard()
        {
            Random rand = new Random();
            int maxXpos = pbCanvas.Size.Width / Setting.width;
            int maxYpos = pbCanvas.Size.Height / Setting.height;
            int newX = rand.Next(0, maxXpos);
            int newY = rand.Next(0, maxYpos);

            food = new Circles { X = newX, Y = newY };
            
        }

        private void eat()
        {
            
            Circles body = new Circles
            {
                X = Snake[Snake.Count - 1].X,
                Y = Snake[Snake.Count  - 1].Y

            };
            Snake.Add(body);

            Setting.points++;
            pointsBox.Text = "Points: " + Setting.points;
            addBlocktoBoard();
        }

        private void endGame()
        {
            Setting.gameOVer = true;
            Setting.score = Setting.points;
            Setting.points = 0;
            
            
        }

        private void nameTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void enterButton_Click(object sender, EventArgs e)
        {
            string playerEnteredName = nameTextBox.Text;

        }
    }
}
